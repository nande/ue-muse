# ue-muse

Unreal plugin to use muse, by connecting through osc with mind monitor.

This plugin was developed as part of this game, check it out 
https://codeberg.org/nande/LifeDev


Requirements:
* a muse headband
* mind monitor app and a compatible mobile phone
* a local network between unreal and the mobile phone

Usage:
* copy the folder to the Plugins folder of your project
* subscribe to the delegates you want to use
* maybe set the address and port on your app and in mind-monitor
* see MuseActor for an example
